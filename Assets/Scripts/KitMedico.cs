﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitMedico : MonoBehaviour
{
    private Tags Tags;
    private int quantidadeDeCura = 15;
    private int tempoDestruir = 5;

    private void Start()
    {
        Tags = GetComponent<Tags>();
        Destroy(gameObject, tempoDestruir);
    }

    private void OnTriggerEnter(Collider objetoDeColisao)
    {
        if (objetoDeColisao.tag == Tags.Jogador)
        {
            objetoDeColisao.GetComponent<ControlaJogador>().CurarVida(quantidadeDeCura);
            Destroy(gameObject);
        }
    }
}

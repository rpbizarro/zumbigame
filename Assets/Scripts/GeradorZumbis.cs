﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeradorZumbis : MonoBehaviour
{

    public GameObject Zumbi;
    float contadorTempo = 0;
    public float TempoGerarZumbi = 1;
    private float distanciaDeGeracao = 3;
    public LayerMask LayerZumbi;
    private float DistanciaDoJogadorParaGeracao = 20;
    private GameObject jogador;
    private Tags Tags;
    private float quantidadeMaximaDeZumbisVivos = 2;
    private float quantidadeDeZumbisVivos;
    private float tempoProximoAumentoDeDificuldade = 30;
    private float contadorDeAumentarDificuldade = 0;

    void Start()
    {
        Tags = GetComponent<Tags>();
        jogador = GameObject.FindWithTag(Tags.Jogador);
        for (int i = 0; i < quantidadeMaximaDeZumbisVivos; i++)
        {
            StartCoroutine(GerarNovoZumbi());
        }
        contadorDeAumentarDificuldade = tempoProximoAumentoDeDificuldade;
    }

    void Update()
    {
        bool possoGerarZumbisPelaDistancia = Vector3.Distance(transform.position, jogador.transform.position) > DistanciaDoJogadorParaGeracao;

        if (possoGerarZumbisPelaDistancia && quantidadeDeZumbisVivos < quantidadeMaximaDeZumbisVivos)
        {
            contadorTempo += Time.deltaTime;

            if (contadorTempo >= TempoGerarZumbi)
            {
                StartCoroutine(GerarNovoZumbi());
                contadorTempo = 0;
            }
        }

        if (Time.timeSinceLevelLoad > contadorDeAumentarDificuldade)
        {
            contadorDeAumentarDificuldade = Time.timeSinceLevelLoad + tempoProximoAumentoDeDificuldade;
            quantidadeMaximaDeZumbisVivos++;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, distanciaDeGeracao);
    }

    IEnumerator GerarNovoZumbi()
    {
        Vector3 posicaoDeCriacao = AleatorizarPosicao();
        Collider[] colisores = Physics.OverlapSphere(posicaoDeCriacao, 1, LayerZumbi);

        while(colisores.Length > 0)
        {
            posicaoDeCriacao = AleatorizarPosicao();
            colisores = Physics.OverlapSphere(posicaoDeCriacao, 1, LayerZumbi);
            yield return null;
        }
        ControlaZumbi zumbi = Instantiate(Zumbi, posicaoDeCriacao, transform.rotation).GetComponent<ControlaZumbi>();
        zumbi.meuGerador = this;
        quantidadeDeZumbisVivos++;
    }

    Vector3 AleatorizarPosicao()
    {
        Vector3 posicao = Random.insideUnitSphere * distanciaDeGeracao;
        posicao += transform.position;
        posicao.y = transform.position.y;

        return posicao;
    }

    public void DiminuirQuantidadeZumbisVivos()
    {
        quantidadeDeZumbisVivos--;
    }
}

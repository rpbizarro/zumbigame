﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{

    public float Velocidade = 20;
    private Rigidbody rigidbodyBala;
    private Tags Tags;
    private int danoDoTiro = 1;

    private void Start() 
    {
        rigidbodyBala = GetComponent<Rigidbody>();
        Tags = GetComponent<Tags>();
    }

    void FixedUpdate()
    {
        rigidbodyBala.MovePosition(
            rigidbodyBala.position + 
            transform.forward * Velocidade * Time.deltaTime);
    }

    void OnTriggerEnter(Collider objetoDeColisao)
    {
        if(objetoDeColisao.tag == Tags.Inimigo)
        {
            objetoDeColisao.GetComponent<ControlaZumbi>().TomarDano(danoDoTiro);
        }

        Destroy(gameObject);
    }
}
